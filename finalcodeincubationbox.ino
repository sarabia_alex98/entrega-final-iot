#include "ThingsBoard.h"
#include <WiFi.h>
#include "DHT.h"
#define DHTPIN  25      
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);
#define led1 33  //R
#define led3 32 //G
#define led2 27 //B

#define ENABLE 14 // relé
#define WIFI_AP             "VTR-7586200"
#define WIFI_PASSWORD       "m3wHchx9tfth"
#define TOKEN               "Gl1XKYLpDinDuHUL8zAI"


#define THINGSBOARD_SERVER  "iot.ceisufro.cl"
#define SERIAL_DEBUG_BAUD   115200
WiFiClient espClient;
ThingsBoard tb(espClient);
int status = WL_IDLE_STATUS;

void setup() {
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(ENABLE, OUTPUT);
  Serial.begin(SERIAL_DEBUG_BAUD);
  WiFi.begin(WIFI_AP, WIFI_PASSWORD);
  delay(1000);
  dht.begin();
  InitWiFi();
}

void loop() {
  if (WiFi.status() != WL_CONNECTED) {
    reconnect();
  }
  if (!tb.connected()) {
    Serial.print("Connecting to: ");
    Serial.print(THINGSBOARD_SERVER);
    Serial.print(" with token ");
    Serial.println(TOKEN);
    if (!tb.connect(THINGSBOARD_SERVER, TOKEN)) {
      Serial.println("Failed to connect");
      return;
    }
  }
  Serial.println("Sending data...");
  float h = dht.readHumidity();      
  float t = dht.readTemperature();    
  Serial.println("Temperatura: ");
  Serial.println(t);
  Serial.println("Humedad: ");
  Serial.println(h);






  float limiteInferior=15.0; //esto tiene que recibirse de afuera...
  float limiteSuperior=20.0; //esto tiene que recibirse de afuera...
 
  // Comprobamos si hubo algún error en la lectura
  if (isnan(t)) {
    Serial.println("Error obteniendo los datos del sensor DHT11");
    return;
  }

  if(t<limiteInferior){
  //azul
  digitalWrite(led1, LOW);
  digitalWrite(led2, HIGH);
  digitalWrite(led3, LOW);
  digitalWrite(ENABLE,HIGH);
  delay(500);

  }else if(t>limiteSuperior){
    //Rojo
  digitalWrite(led1, HIGH);
  digitalWrite(led2, LOW);
  digitalWrite(led3, LOW);
  digitalWrite(ENABLE,LOW);
  delay(500);
  }else{
    digitalWrite(led1, LOW);
    digitalWrite(led2, LOW);
    digitalWrite(led3, HIGH);
    digitalWrite(ENABLE,HIGH);
    delay(500);
  } 

  tb.sendTelemetryInt("Temperatura", t);
  tb.sendTelemetryFloat("Humedad", h);
  delay(5000);
  tb.loop();
}

void InitWiFi()
{
  Serial.println("Connecting to AP ...");
  WiFi.begin(WIFI_AP, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("Connected to AP");
}

void reconnect() {
  status = WiFi.status();
  if ( status != WL_CONNECTED) {
    WiFi.begin(WIFI_AP, WIFI_PASSWORD);
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
    Serial.println("Connected to AP");
  }
}